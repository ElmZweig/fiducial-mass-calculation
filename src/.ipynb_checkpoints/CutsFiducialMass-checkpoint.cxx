#include "CutsFiducialMass.h"
#include "ConfigSvc.h"

CutsFiducialMass::CutsFiducialMass(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsFiducialMass::~CutsFiducialMass()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsFiducialMass::FiducialMassCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}
