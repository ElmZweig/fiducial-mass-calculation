#include "FiducialMass.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsFiducialMass.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

// Constructor
FiducialMass::FiducialMass()
    : Analysis()
{
    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    // Load in the Single Scatters Branch
    m_event->IncludeBranch("ss");
    m_event->Initialize();
    ////////

    // Setup logging
    logging::set_program_name("FiducialMass Analysis");

    // Setup the analysis specific cuts.
    m_cutsFiducialMass = new CutsFiducialMass(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();
}

// Destructor
FiducialMass::~FiducialMass()
{
    delete m_cutsFiducialMass;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void FiducialMass::Initialize()
{
    INFO("Initializing FiducialMass Analysis");
}

// Execute() - Called once per event.
void FiducialMass::Execute()
{

    // Make a variable from the contents of the data
    //  prevents having to write full ' (*event->Branch)->variableName ' structure many times in the analysis
    bool isXe131m = false;
    int numSS = (*m_event->m_singleScatter)->nSingleScatters;
    
    Double_t cS2Area = log10((*m_event->m_singleScatter)->correctedS2Area_phd);
    Double_t cS1Area = (*m_event->m_singleScatter)->correctedS1Area_phd;

    
    Double_t R2 = ((*m_event->m_singleScatter)->x_cm)*((*m_event->m_singleScatter)->x_cm) + ((*m_event->m_singleScatter)->y_cm)*((*m_event->m_singleScatter)->y_cm);
    
    m_hists->BookFillHist("s1_log10s2_phd_nocut", 300, 0., 3000., 300, 1., 7., cS1Area, cS2Area);

    const float W = 13.5e-3;
    const float g1 = 0.108;
    const float g2 = 40.4;
    // testing git
    Double_t Eee = W*(cS1Area/g1+(pow(10,cS2Area))/g2);
    
    //apply cut
    if( (Eee > 150 && Eee < 175)
        // (cS2Area <= ( -0.000345889387026*cS1Area + 6.24942329702)) && //&& //line 1
        // (cS2Area >= ( 0.289107761747*cS1Area -287.043319224)) && //line2
        // (cS2Area >= ( -0.0578251196982*cS1Area + 43.4260175935)) && //line3
        // (cS2Area >= ( -0.000337166399084*cS1Area + 5.61215068997)) //line4
        //(R2  < 3500.0)
        ){ isXe131m = true;}
    
    // if there is a single scatter in the event then plot the S1 pulse area
    if (numSS > 0 && isXe131m) {
        m_hists->BookFillHist("s1Area_phd", 300, 0., 3000., (*m_event->m_singleScatter)->s1Area_phd);
        m_hists->BookFillHist("correctedS1Area_phd", 120, 200., 1400., cS1Area);
        m_hists->BookFillHist("s2Area_phd_log10", 100, 4.1, 5.9, cS2Area);
        m_hists->BookFillHist("s1_log10s2_phd", 300, 0., 3000., 300, 1., 7., cS1Area, cS2Area);
        m_hists->BookFillHist("driftTime_ns", 1400, -1400000., 0., -0.001*(*m_event->m_singleScatter)->driftTime_ns);
        m_hists->BookFillHist("R2", 600, 0., 6000., R2);
        m_hists->BookFillHist("R2_driftTime", 200, 0., 6000., 1400, -1400., 0., R2, -0.001*(*m_event->m_singleScatter)->driftTime_ns);
        
        //(R2  < 4750.0)
        
        // TCutG cx("cx_Xe131m", 5);
        // cx.SetPoint(0, 415, 5.40);
        // cx.SetPoint(1, 900, 4.94);
        // cx.SetPoint(2, 1040, 5.25);
        // cx.SetPoint(3, 500, 5.62);
        // cx.SetPoint(4, 415, 5.40);
        // return m_event->ss.Eee > 150 && m_event->ss.Eee < 175 && cx.IsInside(m_event->ss.cS1area, m_event->ss.log10(S2area));;


    }
}

// Finalize() - Called once after event loop.
void FiducialMass::Finalize()
{
    INFO("Finalizing FiducialMass Analysis");
    
    TCanvas *c1 = new TCanvas();
    
    m_hists->GetHistFromMap("s1_log10s2_phd")->GetXaxis()->SetTitle("cS1 [phd]");
    m_hists->GetHistFromMap("s1_log10s2_phd")->GetYaxis()->SetTitle("Log10(cS2 [phd])");
    gPad->SetLogz();
    m_hists->GetHistFromMap("s1_log10s2_phd")->Draw("COLZ");
    m_hists->GetHistFromMap("s1_log10s2_phd")->SetStats(0);
    //m_hists->GetHistFromMap("s1_log10s2_phd")->SetLogz();
    
    c1->SaveAs("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/s1_log10s2_phd.png");
    
    TCanvas *c2 = new TCanvas();
    
    m_hists->GetHistFromMap("R2_driftTime")->GetXaxis()->SetTitle("r2 [cm2]");
    m_hists->GetHistFromMap("R2_driftTime")->GetYaxis()->SetTitle("drift [us]");
    gPad->SetLogz();
    m_hists->GetHistFromMap("R2_driftTime")->Draw("COLZ");
    m_hists->GetHistFromMap("R2_driftTime")->SetStats(0);
    
    c2->SaveAs("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/R2_driftTime.png");
    
    TCanvas *c3 = new TCanvas();
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->GetXaxis()->SetTitle("cS1 [phd]");
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->GetYaxis()->SetTitle("Log10(cS2 [phd])");
    gPad->SetLogz();
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->Draw("COLZ");
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->SetStats(0);
    
    c3->SaveAs("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/s1_log10s2_phd_nocut.png");
    
}
