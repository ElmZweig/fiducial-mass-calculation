#include "EventBase.h"
#include "Analysis.h"
#include "HistSvc.h"
#include "VisSvc.h"
#include "Logger.h"
#include "CutsBase.h"
#include "CutsFiducialMass.h"
#include "ConfigSvc.h"
#include "FiducialMass.h"
#include "TF1.h"
#include "TLine.h"

// Constructor
FiducialMass::FiducialMass()
  : Analysis()
{
  // List the branches required below, see full list here *:::: LINK TO SOFTWARE DOCS  ::::*
  // NOTE : if you try and use a variable in a branch that has NOT been loaded you will get a seg fault
  // there is not a error message for this currently. If you get a seg fault first thing to check 
  // is that you are including the required branch that variable lives on. 
  m_event->IncludeBranch("pulsesTPCHG");
  m_event->IncludeBranch("pulsesODHG");
  m_event->IncludeBranch("ms");
  m_event->IncludeBranch("ss");
  ////////

  m_event->Initialize();
  m_cuts->sr1()->Initialize();
  m_cuts->sr1()->SetHotspotExclusions(std::string(std::getenv("ALPACA_TOPDIR"))+"/modules/FiducialMass/exclusions/SR1_test_exclude_ts.txt");
  // Setup logging
  logging::set_program_name("Fiducial Mass Analysis");
  // Logging level: error = 1, warning = 2, info = 3, debug = 4, verbose = 5
  
  m_cutsFiducialMass = new CutsFiducialMass(m_event);
  
  //create a config instance, this can be used to call the config variables. 
  m_conf = ConfigSvc::Instance();
}

// Destructor
FiducialMass::~FiducialMass() {
  delete m_cutsFiducialMass;
}

// Called before event loop
void FiducialMass::Initialize() {
  INFO("Initializing Fiducial Mass Analysis");

  int doVis = m_config->configFileVarMap["produceVisObjects"];
  if (doVis == 1){
    m_vis->InitParaviewPMTPos();
    m_vis->InitParaviewODPMTPos();
  }
}

// Called once per event
void FiducialMass::Execute() {

 // Make a variable from the contents of the data
    //  prevents having to write full ' (*event->Branch)->variableName ' structure many times in the analysis
    bool isXe = false;
    bool isXe_s1s2cut = false;
    const int source = 1;//1 //0 for Xe131m, 1 for Xe127
    int numSS = (*m_event->m_singleScatter)->nSingleScatters;
    m_cuts->sr1()->InitializeETrainVetoInEvent();
    const bool cxSS = (*m_event->m_singleScatter)->nSingleScatters;
    const bool cxMUONVETO = m_cuts->sr1()->TPCMuonVeto();
    const bool cxHSX = m_cuts->sr1()->HotspotExclusion();
    const bool cxSTINGER = cxSS && m_cuts->sr1()->Stinger((*m_event->m_singleScatter)->s1PulseID);
    const bool cxHSC = cxSS && m_cuts->sr1()->PassHSCCut((*m_event->m_singleScatter)->s1PulseID);
    const bool cxFCRC = /*m_cuts->sr1()->FiducialR() && */m_cuts->sr1()->FieldCageResistorCut();
    
    const bool cxWSDQ = cxMUONVETO  && cxHSX && cxSTINGER/* && cxBUFF */ && cxHSC && cxFCRC;

    float x_pos=  (*m_event->m_singleScatter)->x_cm;
    float y_pos= (*m_event->m_singleScatter)->y_cm;
    
   m_hists->BookFillHist("correctedXY_noFCRC", 150, -80., 80., 150, -80., 80.,x_pos, y_pos);
   if (cxFCRC){
       m_hists->BookFillHist("correctedXY_FCRC", 150, -80., 80., 150, -80., 80.,x_pos, y_pos);//--Select only Single scatter events
   }
    
  if (numSS !=0  && cxWSDQ){ 
   
    Double_t x = (*m_event->m_singleScatter)->x_cm;   
    Double_t cS2Area = log10((*m_event->m_singleScatter)->correctedS2Area_phd);
    Double_t cS1Area = (*m_event->m_singleScatter)->correctedS1Area_phd;
    Double_t driftTime = -0.001*((*m_event->m_singleScatter)->driftTime_ns); //drift_time in us

     float x_corr=  (*m_event->m_singleScatter)->correctedX_cm;
     float y_corr= (*m_event->m_singleScatter)->correctedY_cm;
     Double_t R2_corr =x_corr*x_corr+y_corr*y_corr;
   
     float x_pos=  (*m_event->m_singleScatter)->x_cm;
     float y_pos= (*m_event->m_singleScatter)->y_cm;
     Double_t R2 =x_pos*x_pos+y_pos*y_pos;

     //from Alden: works for lzap 5.4.5
  const float W = 13.5e-3;
    const float g1 = 0.1176;
    const float g2 = 43.56;

   //from GregR presentation: https://docs.google.com/presentation/d/1p2Daj_wsGnTMuYmLQt4IaqP86yTnG8QQ12uEji9A4kU/edit#slide=id.g10d1d57df03_0_42
   //works for lzap 5.4.2
   // const float W = 13.5e-3;
    // const float g1 = 0.1080;
    // const float g2 = 40.4;
   
    Double_t Eee = W*(cS1Area/g1+(pow(10,cS2Area))/g2);
    const float Xe_min = 150.;
    const float Xe_max = 180.;
    // if (source == 0){
    //     const float Xe_min = 150.;
    //     const float Xe_max = 180.;
    // }
    if (source == 1){
    const float Xe_min = 220.; //for Xe127
    const float Xe_max = 250.; //for Xe127
    }

   const float p0 = 72.4403;
    const float p1 = 9.33984e-3;
    const float p2 = 5.06325e-5;
    const float p3 = 1.65361e-7;
    const float p4 = 2.92605e-10;
    const float p5 = 2.53539e-13;
    const float p6 = 8.30075e-17;
   
    Double_t NishatFit = p0 + p1*driftTime + p2*pow(driftTime,2) + p3*pow(driftTime,3) + p4*pow(driftTime,4) + p5*pow(driftTime,5) + p6*pow(driftTime,6);
    Double_t Nishat_Fidradius = NishatFit -4;
      
    if (  fabs(driftTime)<200)
   Nishat_Fidradius = NishatFit -5.2 ;
   if ( fabs(driftTime) >200 && fabs(driftTime) <800)
  Nishat_Fidradius = NishatFit -4.0;
  if  ( fabs(driftTime) >800)
 Nishat_Fidradius = NishatFit -5.0;

   Int_t Nbin_R2=100;//250;
   Double_t min_R2=0.0;
   Double_t max_R2=5500;//6000;
   Int_t Nbin_dT=200;
   Double_t min_dT=-1000.0;
   Double_t max_dT=10;

    //---applying energy cuts to select different signals and background. Size of the 2 ROIs should be equals
   Double_t compton_bckg_minEROI=100.0;
   Double_t compton_bckg_maxEROI=130.0;
   Double_t Xe_minEROI= 150.0;
   Double_t Xe_maxEROI= 180.0;
   // if (source == 0){
   //     Double_t compton_bckg_minEROI=100.0;
   //     Double_t compton_bckg_maxEROI=130.0;
   //     Double_t Xe_minEROI= 150.0;
   //     Double_t Xe_maxEROI= 180.0;
   // }
   if (source == 1){
       Double_t compton_bckg_minEROI=270.; //for Xe127
       Double_t compton_bckg_maxEROI=300.; //for Xe127
       Double_t Xe_minEROI= 220.; //for Xe127
       Double_t Xe_maxEROI= 250.; //for Xe127
   }

  //---Defining different data selection cuts
   Double_t min_dT_cut=-936;//-800;//
   Double_t max_dT_cut=-86;//-200;//
   Double_t cut_s2_xe_min = 100000;//for the new data, for the old data it was 200000;//100000;
   Double_t cut_s2_xe_max=400000;//300000;
   Double_t cut_s1_xe_min = 550;//800
   Double_t cut_s1_xe_max=1000;//1300
   // if (source == 0){
   //     Double_t cut_s1_xe_min = 550;//800
   //     Double_t cut_s1_xe_max=1000;//1300
   // }
   if (source == 1){
       Double_t cut_s1_xe_min = 800; //for Xe127
       Double_t cut_s1_xe_max=1300; //for Xe127
   }
  Double_t  cS2Area_pow = pow(10,cS2Area);
      
   //--no drift_time cut
    m_hists->BookFillHist("R2_vs_driftTime_nocut_alltime", Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT, R2, driftTime);
    m_hists->BookFillHist("s1_log10s2_phd_nocut_alltime", 200, 100., 3000., 200, 1., 7., cS1Area, cS2Area);
    m_hists->BookFillHist("correctedXY_cm", 150, -80., 80., 150, -80., 80.,x_pos, y_pos);
  //--applying a  drift_time cut (to remove gas events on top and unphysicall events with drift time larger than max drift_time 950us)
    if  ( driftTime < max_dT_cut && driftTime >=min_dT_cut ) 
    {
    m_hists->BookFillHist("s1_log10s2_phd_nocut", 200, 100., 3000., 200, 1., 7., cS1Area, cS2Area);
    m_hists->BookFillHist("correctedS1Area_phd_nocut", 290, 100., 3000., cS1Area);
    m_hists->BookFillHist("R2_vs_driftTime_nocut", Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT, R2, driftTime);
    m_hists->BookFillHist("R2_nocut", Nbin_R2, min_R2, max_R2,  R2);
    if( x >= 0.) //applying the x_pos cut to use 1 side of the detector without hotspot
  	  m_hists->BookFillHist("Eee", 150, 50., 300., Eee); //	  m_hists->BookFillHist("Eee", 270, 50., 500., Eee);
  }
    
    //----applyin cut to select Xe131m energy population from the logS2 vs s1 plot
    if (source == 0){
      if (
        (cS2Area_pow >= cut_s2_xe_min &&  cS2Area_pow <=cut_s2_xe_max) && //turn on for Xe131m
        ( cS1Area >= cut_s1_xe_min  && cS1Area <=cut_s1_xe_max) //turn on for Xe131m
          // (cS2Area >= ( 0.00308742583964*cS1Area + 0.82900350931)) && //turn on for Xe127
          // (cS2Area <= ( 0.000761592755416*cS1Area + 4.94075063379)) //turn on for Xe127
         )
       { isXe_s1s2cut = true;}
    }
    if (source == 1){
        if (
        // (cS2Area_pow >= cut_s2_131mxe_min &&  cS2Area_pow <=cut_s2_131mxe_max) && //turn on for Xe131m
        // ( cS1Area >= cut_s1_131mxe_min  && cS1Area <=cut_s1_131mxe_max) //turn on for Xe131m
          (cS2Area >= ( 0.00308742583964*cS1Area + 0.82900350931)) && //turn on for Xe127
          (cS2Area <= ( 0.000761592755416*cS1Area + 4.94075063379)) //turn on for Xe127
         )
       { isXe_s1s2cut = true;}
    }
    
    if (source == 0){
        if( (Eee > Xe_min && Eee < Xe_max) &&
            ( driftTime < max_dT_cut && driftTime >=min_dT_cut ) &&
            (cS2Area_pow >= cut_s2_xe_min &&  cS2Area_pow <=cut_s2_xe_max) && //turn on for Xe131m
            ( cS1Area >= cut_s1_xe_min  && cS1Area <=cut_s1_xe_max) //turn on for Xe131m
               // (cS2Area >= ( 0.00308742583964*cS1Area + 0.82900350931)) && //turn on for Xe127m
              // (cS2Area <= ( 0.000761592755416*cS1Area + 4.94075063379)) //turn on for Xe127m
        ){ isXe = true;}
    }
    if (source == 1){
        if( (Eee > Xe_min && Eee < Xe_max) &&
            ( driftTime < max_dT_cut && driftTime >=min_dT_cut ) &&
            // (cS2Area_pow >= cut_s2_131mxe_min &&  cS2Area_pow <=cut_s2_131mxe_max) && //turn on for Xe131m
            // ( cS1Area >= cut_s1_131mxe_min  && cS1Area <=cut_s1_131mxe_max) //turn on for Xe131m
               (cS2Area >= ( 0.00308742583964*cS1Area + 0.82900350931)) && //turn on for Xe127m
              (cS2Area <= ( 0.000761592755416*cS1Area + 4.94075063379)) //turn on for Xe127m
        ){ isXe = true;}
    }

    if( driftTime < max_dT_cut && driftTime >=min_dT_cut  && x>=0) {

    if( Eee > compton_bckg_minEROI && Eee < compton_bckg_maxEROI){
    m_hists->BookFillHist("R2_vs_driftTime_compBG", Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT, R2, driftTime);
    m_hists->BookFillHist("R2_compBG", Nbin_R2, min_R2, max_R2,  R2);
    	if (R2 <= pow(Nishat_Fidradius,2) ){
    	m_hists->BookFillHist("R2_vs_driftTime_compBG_inner", Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT, R2, driftTime);
   	 m_hists->BookFillHist("R2_compBG_inner", Nbin_R2, min_R2, max_R2,  R2);
    	 }
     }
    
    if( Eee > Xe_minEROI && Eee < Xe_maxEROI && isXe_s1s2cut  ){
    m_hists->BookFillHist("R2_vs_driftTime_signal+compBG", Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT, R2, driftTime);
    m_hists->BookFillHist("R2_signal+compBG", Nbin_R2, min_R2, max_R2,  R2);
	if ( R2 <= pow(Nishat_Fidradius,2) ){
   	 m_hists->BookFillHist("R2_vs_driftTime_signal+compBG_inner", Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT, R2, driftTime);
   	 m_hists->BookFillHist("R2_signal+compBG_inner", Nbin_R2, min_R2, max_R2,  R2);
  	  }
     }

}


    if (numSS >0 && isXe) {
        m_hists->BookFillHist("R2", Nbin_R2, min_R2, max_R2,  R2);
        m_hists->BookFillHist("R2_vs_driftTime", Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT, R2, driftTime);
       m_hists->BookFillHist("s1_log10s2_phd_Xe", 200, 100., 3000., 200, 1., 7., cS1Area, cS2Area); //checking if event population for Xe131 was properly selected
    }
    
    // if there is a single scatter in the event then plot the S1 pulse area
    if (numSS >0 && isXe) {
        m_hists->BookFillHist("s1Area_phd", 200, 100., 3000., (*m_event->m_singleScatter)->s1Area_phd);
        m_hists->BookFillHist("correctedS1Area_phd", 200, 100., 3000., cS1Area);
        m_hists->BookFillHist("s2Area_phd_log10", 60, 4.1, 5.9, cS2Area);
        m_hists->BookFillHist("s1_log10s2_phd", 200, 100., 3000., 200, 1., 7., cS1Area, cS2Area);
        m_hists->BookFillHist("s1_log10s2_phd_focus", 100, 200., 1400., 60, 4.1, 5.9, cS1Area, cS2Area);
        m_hists->BookFillHist("driftTime_ns", Nbin_dT, min_dT, max_dT, driftTime);
        m_hists->BookFillHist("R2_inner",Nbin_R2, min_R2, max_R2,  R2);
        m_hists->BookFillHist("R2_vs_driftTime_inner", Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT, R2, driftTime);
     }
    
        m_hists->BookFillHist("s1_log10s2_phd_outer", 200, 100., 3000., 200, 1., 7., cS1Area, cS2Area);
        m_hists->BookFillHist("R2_vs_driftTime_outer", Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT, R2, driftTime);
        
    } // end of if Single scatter
    m_cuts->sr1()->DeleteInactiveProgenitors();

}


// Finalize() - Called once after event loop.
void FiducialMass::Finalize() {
    INFO("Finalizing FiducialMass Analysis");

    const float p0 = 72.4403;
    const float p1 = 9.33984e-3;
    const float p2 = 5.06325e-5;
    const float p3 = 1.65361e-7;
    const float p4 = 2.92605e-10;
    const float p5 = 2.53539e-13;
    const float p6 = 8.30075e-17;
    float rad_inner = 0.;

    vector<Double_t> fidRadius;
    vector<Double_t> fidRadius_inner;
    vector<Double_t> fidTime;
    Printf("Histos made");
    Int_t dT = 950;
    for (Int_t i=0;i<dT;i++) {
     fidTime.push_back(i-dT);
     fidRadius.push_back(pow(p0 + p1*(i-dT) + p2*pow(i-dT,2) + p3*pow(i-dT,3) + p4*pow(i-dT,4) + p5*pow(i-dT,5) + p6*pow(i-dT,6),2));
     if ( (i-dT)>-200){
         rad_inner = -5.2;
         }
     if ( (i-dT)<=-200 && (i-dT)>=-800){
         rad_inner = -4.0;
     }
     if ( (i-dT)<-800){
         rad_inner = -5.0;
     }
     fidRadius_inner.push_back(pow(p0 + rad_inner + p1*(i-dT) + p2*pow(i-dT,2) + p3*pow(i-dT,3) + p4*pow(i-dT,4) + p5*pow(i-dT,5) + p6*pow(i-dT,6),2));
    }
    Printf("Histos filled");
    
    TGraph *gr = new TGraph(dT,&fidRadius[0],&fidTime[0]);
    TGraph *gr_inner = new TGraph(dT,&fidRadius_inner[0],&fidTime[0]);
    gr->SetLineColor(2);
    gr_inner->SetLineColor(2);
    gr->SetLineWidth(2);
    gr_inner->SetLineWidth(2);

    const int binNum = 200;
    const float quadmin = 100.;
    const float quadmax = 150.;
    const float linmin = quadmax;
    const float linmax = 300.;
    
    //change datarange to the numbers of runs analyzed, for multiple dataset calculations
    
    // string datarange = "7223-7238";
    string datarange = "6771-6813";
    // string datarange = "6726-6762";
    
    TF1 *quadfit = new TF1 ("quadfit", "[0] + [1]*x + [2]*x*x", 100., 150.);
    TF1 *quadfit2 = new TF1("quadfit2", "[0]", 270., 300.);
    TF1 *gausfit = new TF1("gausfit", "gaus", 165., 185.);
    TF1 *gausfit2 = new TF1("gausfit2", "gaus", 215., 235.);
    TF1 *gausfit3 = new TF1("gausfit3", "gaus", 240., 265.);
    
    TCanvas *c1 = new TCanvas();
    
    m_hists->GetHistFromMap("s1_log10s2_phd")->GetXaxis()->SetTitle("cS1 [phd]");
    m_hists->GetHistFromMap("s1_log10s2_phd")->GetYaxis()->SetTitle("Log10(cS2 [phd])");
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->GetXaxis()->SetTitle("cS1 [phd]");
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->GetYaxis()->SetTitle("Log10(cS2 [phd])");
    gPad->SetLogz();
    m_hists->GetHistFromMap("s1_log10s2_phd")->SetStats(0);
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->SetStats(0);
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->SetMarkerColor(46);
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->Draw();
    m_hists->GetHistFromMap("s1_log10s2_phd")->Draw("same COLZ");
    //m_hists->GetHistFromMap("s1_log10s2_phd")->SetLogz();
    
    c1->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/s1_log10s2_phd.png").c_str());
    
    TCanvas *c2 = new TCanvas();
    
    m_hists->GetHistFromMap("R2_vs_driftTime_nocut")->GetXaxis()->SetTitle("r2 [cm2]");
    m_hists->GetHistFromMap("R2_vs_driftTime_nocut")->GetYaxis()->SetTitle("drift [us]");
    gPad->SetLogz();
    m_hists->GetHistFromMap("R2_vs_driftTime_nocut")->Draw("COLZ");
    m_hists->GetHistFromMap("R2_vs_driftTime_nocut")->SetStats(0);
    
    c2->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/R2_driftTime.png").c_str());
    
    TCanvas *c3 = new TCanvas();
    m_hists->GetHistFromMap("s1_log10s2_phd")->GetXaxis()->SetTitle("cS1 [phd]");
    m_hists->GetHistFromMap("s1_log10s2_phd")->GetYaxis()->SetTitle("Log10(cS2 [phd])");
    gPad->SetLogz();
    m_hists->GetHistFromMap("s1_log10s2_phd")->Draw("COLZ");
    m_hists->GetHistFromMap("s1_log10s2_phd")->SetStats(0);
    
    c3->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/s1_log10s2_phd_isolate.png").c_str());
    
    TCanvas *c4 = new TCanvas();
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->GetXaxis()->SetTitle("cS1 [phd]");
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->GetYaxis()->SetTitle("Log10(cS2 [phd])");
    gPad->SetLogz();
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->Draw("COLZ");
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut")->SetStats(0);
    
    c4->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/s1_log10s2_phd_nocut.png").c_str());
    
    TCanvas *c5 = new TCanvas();
    m_hists->GetHistFromMap("s1_log10s2_phd_focus")->GetXaxis()->SetTitle("cS1 [phd]");
    m_hists->GetHistFromMap("s1_log10s2_phd_focus")->GetYaxis()->SetTitle("Log10(cS2 [phd])");
    gPad->SetLogz();
    m_hists->GetHistFromMap("s1_log10s2_phd_focus")->Draw("COLZ");
    m_hists->GetHistFromMap("s1_log10s2_phd_focus")->SetStats(0);
    
    c5->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/s1_log10s2_phd_focus.png").c_str());
    
    TCanvas *c6 = new TCanvas();
    m_hists->GetHistFromMap("correctedS1Area_phd")->GetXaxis()->SetTitle("cS1 [phd]");
    m_hists->GetHistFromMap("correctedS1Area_phd")->GetYaxis()->SetTitle("Entries");
    m_hists->GetHistFromMap("correctedS1Area_phd")->Draw();
    m_hists->GetHistFromMap("correctedS1Area_phd")->SetStats(0);
    
    c6->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/correctedS1Area_phd.png").c_str());
    
    TCanvas *c7 = new TCanvas();
    m_hists->GetHistFromMap("correctedS1Area_phd_nocut")->GetXaxis()->SetTitle("cS1 [phd]");
    m_hists->GetHistFromMap("correctedS1Area_phd_nocut")->GetYaxis()->SetTitle("Entries");
    
    m_hists->GetHistFromMap("correctedS1Area_phd_nocut")->Draw();
    m_hists->GetHistFromMap("correctedS1Area_phd_nocut")->SetStats(0);
    
    c7->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/correctedS1Area_phd_nocut.png").c_str());
    
    TCanvas *c8 = new TCanvas();
    m_hists->GetHistFromMap("Eee")->GetXaxis()->SetTitle("Energy [keV_ee]");
    m_hists->GetHistFromMap("Eee")->GetYaxis()->SetTitle("Entries");
    m_hists->GetHistFromMap("Eee")->GetXaxis()->SetRangeUser(75,300);
    m_hists->GetHistFromMap("Eee")->SetStats(0);
    
    gPad->SetLogy();
    
    TF1 *expandedfit = new TF1("expandedfit", "[0] + [1]*x + [2]*x*x", quadmin, quadmax);
    TF1 *expandedfit2 = new TF1("expandedfit2", "[0]", linmin, linmax);
    // TF1 *expandedgausfit = new TF1("expandedgausfit", "gaus(0)+gaus(3)+gaus(6)", 100., 300.);
    
    expandedfit->SetLineColor(1);
    expandedfit->SetParameter(0, quadfit->GetParameter(0));
    expandedfit->SetParameter(1, quadfit->GetParameter(1));
    expandedfit->SetParameter(2, quadfit->GetParameter(2));
    // expandedfit->SetParameter(3, quadfit->GetParameter(3));
    // expandedfit->SetParameter(4, quadfit->GetParameter(4));
 
    expandedfit2->SetLineColor(2);
    expandedfit2->SetLineStyle(7);
    expandedfit2->SetParameter(0, quadfit2->GetParameter(0));
    // expandedfit2->SetParameter(3, quadfit2->GetParameter(3));
    // expandedfit2->SetParameter(4, quadfit2->GetParameter(4));
    
    // m_hists->GetHistFromMap("Eee")->GetYaxis()->SetRangeUser(30,500);
    m_hists->GetHistFromMap("Eee")->Draw();

   Double_t compton_bckgXe131m_minEROI=100.0;
   Double_t compton_bckgXe131m_maxEROI=130.0;
   Double_t Xe131m_minEROI= 150.0;
   Double_t Xe131m_maxEROI= 180.0;

   Double_t compton_bckgXe127_minEROI=270.;
   Double_t compton_bckgXe127_maxEROI=300.;
   Double_t Xe127m_minEROI= 220.;
   Double_t Xe127m_maxEROI= 250.;


   TLine *l1 = new TLine(compton_bckgXe131m_minEROI,0.,compton_bckgXe131m_minEROI,8e3);
   l1->SetLineColor(1);
   l1->SetLineWidth(2);
   l1->SetLineStyle(9);
   l1->Draw("same");
   TLine *l2 = new TLine(compton_bckgXe131m_minEROI+30,0.,compton_bckgXe131m_minEROI+30,8e3);
   l2->SetLineColor(1);
   l2->SetLineWidth(2);
   l2->SetLineStyle(9);
   l2->Draw("same");

   TLine *l3 = new TLine(Xe131m_minEROI,0.,Xe131m_minEROI,8e3);
   l3->SetLineColor(6);
   l3->SetLineWidth(2);
   l3->SetLineStyle(9);
   l3->Draw("same");

   TLine *l4 = new TLine(Xe131m_minEROI+30,0.,Xe131m_minEROI+30,8e3);
   l4->SetLineColor(6);
   l4->SetLineWidth(2);
   l4->SetLineStyle(9);
   l4->Draw("same");

   TLine *l_Xe131m = new TLine(164,0.,164,8e3);
   l_Xe131m->SetLineColor(2);
   l_Xe131m->SetLineWidth(2);
   l_Xe131m->Draw("same");

     TLine *l_Xe127m = new TLine(236,0.,236,8e3);
   l_Xe127m->SetLineColor(2);
   l_Xe127m->SetLineWidth(2);
   l_Xe127m->Draw("same");
    // expandedfit->Draw("same");
    // expandedfit2->Draw("same");
    
   TLine *l11 = new TLine(compton_bckgXe127_minEROI,0.,compton_bckgXe127_minEROI,8e3);
   l11->SetLineColor(1);
   l11->SetLineWidth(2);
   l11->SetLineStyle(9);
   l11->Draw("same");
   TLine *l21 = new TLine(compton_bckgXe127_minEROI+30,0.,compton_bckgXe127_minEROI+30,8e3);
   l21->SetLineColor(1);
   l21->SetLineWidth(2);
   l21->SetLineStyle(9);
   l21->Draw("same");

   TLine *l31= new TLine(Xe127m_minEROI,0.,Xe127m_minEROI,8e3);
   l31->SetLineColor(6);
   l31->SetLineWidth(2);
   l31->SetLineStyle(9);
   l31->Draw("same");

   TLine *l41 = new TLine(Xe127m_minEROI+30,0.,Xe127m_minEROI+30,8e3);
   l41->SetLineColor(6);
   l41->SetLineWidth(2);
   l41->SetLineStyle(9);
   l41->Draw("same");

    c8->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/Eee.png").c_str());
    
    TH1F *noCompton = new TH1F("noCompton", "", binNum, quadmin, linmax);
    TH1F *Eee_subtract = new TH1F("Eee_subtract", "", binNum, quadmin, linmax);
    
    
    for (int i=0;i<(quadmax-quadmin);i++)
    {
       float xval = m_hists->GetHistFromMap("Eee")->GetBinCenter(i);
       float y = expandedfit->Eval(xval);
        // cout<< "xval  =.  "<<xval<<"   y= "<<y<<endl;
       noCompton->SetBinContent(i,y);
    }
    
    for (int i=(linmin-quadmin);i<(linmax-quadmin);i++)
    {
        float xval = m_hists->GetHistFromMap("Eee")->GetBinCenter(i);
        float y = expandedfit2->Eval(xval);
        noCompton->SetBinContent(i,y);
    }
    
    for (int i=0;i<binNum;i++)
    {
        float xval = m_hists->GetHistFromMap("Eee")->GetBinContent(i);
        float yval = noCompton->GetBinContent(i);
        Eee_subtract->SetBinContent(i, xval - yval);
        //cout<<" bin content.  "<<Eee_subtract->GetBinContent(i)<<endl;
    }
    
    TCanvas *c9 = new TCanvas();
    // c9->Divide(1,2);
    // c9->cd(1);
    m_hists->GetHistFromMap("Eee")->SetStats(0);
    m_hists->GetHistFromMap("Eee")->Draw();
    expandedfit2->Draw("same");
    gPad->SetLogy();
//     c9->cd(2);
//     Eee_subtract->SetStats(0);
//     Eee_subtract->Draw();
//     gPad->SetLogy();
    
    c9->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/subtractCompton.png").c_str());
    
    TCanvas *c10 = new TCanvas();
    c10->Divide(1,2);
    c10->cd(1);
    m_hists->GetHistFromMap("R2_vs_driftTime_inner")->GetXaxis()->SetTitle("r2 [cm2]");
    m_hists->GetHistFromMap("R2_vs_driftTime_inner")->GetYaxis()->SetTitle("drift [us]");
    m_hists->GetHistFromMap("R2_vs_driftTime_inner")->SetStats(0);
    m_hists->GetHistFromMap("R2_vs_driftTime_inner")->Draw("COLZ");
    gPad->SetLogz();
    c10->cd(2);
    // m_hists->GetHistFromMap("R2_vs_driftTime")->GetXaxis()->SetTitle("r2 [cm2]");
    // m_hists->GetHistFromMap("R2_vs_driftTime")->GetYaxis()->SetTitle("drift [us]");
    // m_hists->GetHistFromMap("R2_vs_driftTime")->SetStats(0);
    // m_hists->GetHistFromMap("R2_vs_driftTime")->Draw("COLZ");
    // gPad->SetLogz();
    // c8->cd(3);
     m_hists->GetHistFromMap("R2_inner") ->SetStats(0);
    m_hists->GetHistFromMap("R2_inner")->GetXaxis()->SetTitle("r2 [cm2]");
    m_hists->GetHistFromMap("R2_inner")->GetYaxis()->SetTitle("drift [us]");
    m_hists->GetHistFromMap("R2_inner")->SetStats(0);
    m_hists->GetHistFromMap("R2_inner")->Draw();
    gPad->SetLogz();
    // c8->cd(4);
    // m_hists->GetHistFromMap("R2")->GetXaxis()->SetTitle("r2 [cm2]");
    // m_hists->GetHistFromMap("R2")->GetYaxis()->SetTitle("drift [us]");
    // m_hists->GetHistFromMap("R2")->SetStats(0);
    // m_hists->GetHistFromMap("R2")->Draw();
    // gPad->SetLogy();
    
    c10->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/R2_Subtraction.png").c_str());
    
    TCanvas *c11 = new TCanvas();
    
    m_hists->GetHistFromMap("R2_vs_driftTime_nocut_alltime")->GetXaxis()->SetTitle("r2 [cm2]");
    m_hists->GetHistFromMap("R2_vs_driftTime_nocut_alltime")->GetYaxis()->SetTitle("drift [us]");
    gPad->SetLogz();
    m_hists->GetHistFromMap("R2_vs_driftTime_nocut_alltime")->Draw("COLZ");
    m_hists->GetHistFromMap("R2_vs_driftTime_nocut_alltime")->SetStats(0);
    
    c11->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/R2_driftTime_alltime.png").c_str());

    TCanvas *c1a1 = new TCanvas();
    c1a1->Divide(2,2);
    c1a1->cd(1);
    m_hists->GetHistFromMap("correctedXY_cm")->GetXaxis()->SetTitle("X [cm]");
    m_hists->GetHistFromMap("correctedXY_cm")->GetYaxis()->SetTitle("Y[cm]");
    gPad->SetLogz();
    m_hists->GetHistFromMap("correctedXY_cm")->Draw("COLZ");
   // m_hists->GetHistFromMap("correctedXY_cm")->SetStats(0);

    c1a1->cd(2);
    m_hists->GetHistFromMap("s1_log10s2_phd_Xe")->Draw("COLZ");
    c1a1->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/XY_position_alltime.png").c_str());


    
    TCanvas *c12 = new TCanvas();
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut_alltime")->GetXaxis()->SetTitle("cS1 [phd]");
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut_alltime")->GetYaxis()->SetTitle("Log10(cS2 [phd])");
    gPad->SetLogz();
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut_alltime")->Draw("COLZ");
    m_hists->GetHistFromMap("s1_log10s2_phd_nocut_alltime")->SetStats(0);
    
    c12->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/s1_log10s2_phd_nocut_alltime.png").c_str());
    

   //---- Subtraction of the Compton background
    TCanvas *c13 = new TCanvas();
    c13->Divide(2,2);
    
   Int_t Nbin_R2=100;//250;
   Double_t min_R2=0.0;
   Double_t max_R2=5500;//6000;
   Int_t Nbin_dT=200;
   Double_t min_dT=-1000.0;
   Double_t max_dT=10;
    TH1D*  R2_Sig = new TH1D( "R2_Sig","",Nbin_R2, min_R2, max_R2);
    TH2* R2_dT_BG = new TH2D( "","",Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT);
   TH2* R2_dT_Sig_BG = new TH2D( "","",Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT);
    

    TH1* R2_dT_BG1 = m_hists->GetHistFromMap("R2_vs_driftTime_compBG");
    R2_dT_BG->Add(R2_dT_BG1, 1.0);
    TH1* R2_BG = m_hists->GetHistFromMap("R2_compBG"); //(1)
    TH1D* BG_proj = R2_dT_BG->ProjectionX();                               //(2)
    //comments (1) and (2) are in principle equal

    TH1* R2_dT_Sig_BG1 = m_hists->GetHistFromMap("R2_vs_driftTime_signal+compBG");
    R2_dT_Sig_BG->Add(R2_dT_Sig_BG1, 1.0);   
    TH1* R2_Sig_BG = m_hists->GetHistFromMap("R2_signal+compBG"); //(1prime)
    TH1D* BG_Sig_proj = R2_dT_Sig_BG->ProjectionX();                                 //(2prime)
    //comments (1prime) and (2prime) are in principle equal

    c13->cd(1);
    gPad->SetLogy();
    R2_Sig_BG->SetStats(0);
   R2_Sig_BG->Draw("HIST");

    c13->cd(2);
    R2_BG->SetStats(0);
    gPad->SetLogy();
    R2_BG->Draw("HIST");

    c13->cd(3);
    R2_Sig->Add(R2_Sig_BG, 1.0);
    R2_Sig->Add( R2_BG, -1.0);
    gPad->SetLogy();
    R2_Sig->SetStats(0);
    R2_Sig->Draw("HIST");

    TH2* R2_dT_Sig = new TH2D( "","",Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT);
     c13->cd(4);    
    R2_dT_Sig->Add( R2_dT_Sig_BG, 1.0);
    R2_dT_Sig->Add( R2_dT_BG, -1.0);
    R2_dT_Sig->SetStats(0);
    R2_dT_Sig->Draw("colz");
    gPad->SetLogz();

    c13->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/R2_Sig.png").c_str());
    cout << " nber of events in 1d histo. "<< "      R2_Sig->Integral()      "<<R2_Sig->Integral()<<"       R2_Sig_BG->Integral()    "<<R2_Sig_BG->Integral()<<"  R2_BG->Integral()    "<< R2_BG->Integral()<<"  R2_Sig->Integral()    "<< R2_Sig->Integral()<< endl;
   
    TCanvas *c14 = new TCanvas();
    m_hists->GetHistFromMap("R2_vs_driftTime_compBG")->Draw();
    c14->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/R2_dT_BG.png").c_str());
    
    TCanvas *c15 = new TCanvas();
    m_hists->GetHistFromMap("R2_vs_driftTime_signal+compBG")->Draw();
    c15->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/R2_dT_Sig_BG.png").c_str());
    
    TCanvas *c16 = new TCanvas();
    c16->Divide(2,2);

    TH1D*  R2_Sig_inner = new TH1D( "","",Nbin_R2, min_R2, max_R2);
    TH1* R2_Sig_BG_inner = m_hists->GetHistFromMap("R2_signal+compBG_inner");
    TH1* R2_BG_inner = m_hists->GetHistFromMap("R2_compBG_inner");

    TH2* R2_dT_Sig_inner = new TH2D( "","",Nbin_R2, min_R2, max_R2,  Nbin_dT, min_dT, max_dT);
    TH1* R2_dT_Sig_BG_inner = m_hists->GetHistFromMap("R2_vs_driftTime_signal+compBG_inner");
    TH1* R2_dT_BG_inner = m_hists->GetHistFromMap("R2_vs_driftTime_compBG_inner");

    c16->cd(1);
    gPad->SetLogy();
    R2_Sig_BG_inner->SetStats(0);
    R2_Sig_BG_inner->Draw("HIST");

    c16->cd(2);
    R2_BG_inner->SetStats(0);
    gPad->SetLogy();
    R2_BG_inner->Draw("HIST");

    c16->cd(3);  
    R2_Sig_inner->Add( R2_Sig_BG_inner, 1);
    R2_Sig_inner->Add(R2_BG_inner, -1.0);
    gPad->SetLogy();
    R2_Sig_inner->SetStats(0);
    R2_Sig_inner->Draw("HIST");
 
   c16->cd(4);    
    R2_dT_Sig_inner->Add( R2_dT_Sig_BG_inner, 1);
    R2_dT_Sig_inner->Add( R2_dT_BG_inner, -1.0);
    R2_dT_Sig_inner->SetStats(0);
    R2_dT_Sig_inner->Draw("colz");
    gr->Draw("same L");
    gr_inner->Draw("same L");
    gPad->SetLogz();
    
    c16->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/R2_Sig_inner.png").c_str());
   double ratio_fidmass = (double) R2_Sig_inner->Integral() /(double)R2_Sig->Integral() ;
   double ratio_fidmass_unc =ratio_fidmass*sqrt(1./ (double) R2_Sig_inner->Integral()  + 1./ (double) R2_Sig->Integral());
    cout << " nber of events in inner 1d histo. "<< "R2_Sig_inner->Integral()     "<<R2_Sig_inner->Integral()<<"   R2_Sig_inner->GetEntries()   "<< R2_Sig_inner->GetEntries()<<   "num of events in inner 2d histo. "<<R2_dT_Sig_inner->GetEntries() <<"  ratio_fidmass    "<<ratio_fidmass<<"+/-"  <<ratio_fidmass_unc<<endl;

   //---Ratio for fiducial mass calculation
    TCanvas *c17 = new TCanvas();
    c17->Divide(1,2);
    c17->cd(1);
    m_hists->GetHistFromMap("R2_vs_driftTime")->GetXaxis()->SetTitle("r2 [cm2]");
    m_hists->GetHistFromMap("R2_vs_driftTime")->GetYaxis()->SetTitle("drift [us]");
    m_hists->GetHistFromMap("R2_vs_driftTime")->SetStats(0);
    m_hists->GetHistFromMap("R2_vs_driftTime")->Draw("COLZ");
    gPad->SetLogz();
    c17->cd(2);
    // m_hists->GetHistFromMap("R2_vs_driftTime")->GetXaxis()->SetTitle("r2 [cm2]");
    // m_hists->GetHistFromMap("R2_vs_driftTime")->GetYaxis()->SetTitle("drift [us]");
    // m_hists->GetHistFromMap("R2_vs_driftTime")->SetStats(0);
    // m_hists->GetHistFromMap("R2_vs_driftTime")->Draw("COLZ");
    // gPad->SetLogz();
    // c8->cd(3);
    m_hists->GetHistFromMap("R2")->GetXaxis()->SetTitle("r2 [cm2]");
    m_hists->GetHistFromMap("R2")->GetYaxis()->SetTitle("drift [us]");
    m_hists->GetHistFromMap("R2")->SetStats(0);
    m_hists->GetHistFromMap("R2")->Draw();
    gPad->SetLogy();
    // c8->cd(4);
    // m_hists->GetHistFromMap("R2")->GetXaxis()->SetTitle("r2 [cm2]");
    // m_hists->GetHistFromMap("R2")->GetYaxis()->SetTitle("drift [us]");
    // m_hists->GetHistFromMap("R2")->SetStats(0);
    // m_hists->GetHistFromMap("R2")->Draw();
    // gPad->SetLogy();
    c17->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/R2subtraction.png").c_str());
    
    TCanvas *c18 = new TCanvas();
    m_hists->GetHistFromMap("s1_log10s2_phd_outer")->GetXaxis()->SetTitle("cS1 [phd]");
    m_hists->GetHistFromMap("s1_log10s2_phd_outer")->GetYaxis()->SetTitle("Log10(cS2 [phd])");
    gPad->SetLogz();
    m_hists->GetHistFromMap("s1_log10s2_phd_outer")->Draw("COLZ");
    m_hists->GetHistFromMap("s1_log10s2_phd_outer")->SetStats(0);
    
    c18->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/s1_log10s2_phd_outer.png").c_str());
    
    TCanvas *c19 = new TCanvas();
    m_hists->GetHistFromMap("R2_vs_driftTime_outer")->GetXaxis()->SetTitle("r2 [cm2]");
    m_hists->GetHistFromMap("R2_vs_driftTime_outer")->GetYaxis()->SetTitle("drift [us]");
    gPad->SetLogz();
    m_hists->GetHistFromMap("R2_vs_driftTime_outer")->SetStats(0);
    m_hists->GetHistFromMap("R2_vs_driftTime_outer")->Draw("COLZ");
    
    c19->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/R2_driftTime_outer.png").c_str());
    
    TCanvas *c20 = new TCanvas();
    c20->Divide(1,2);
    c20->cd(1);
    m_hists->GetHistFromMap("correctedXY_noFCRC")->GetXaxis()->SetTitle("X [cm]");
    m_hists->GetHistFromMap("correctedXY_noFCRC")->GetYaxis()->SetTitle("Y [cm]");
    gPad->SetLogz();
    m_hists->GetHistFromMap("correctedXY_noFCRC")->SetStats(0);
    m_hists->GetHistFromMap("correctedXY_noFCRC")->Draw("COLZ");
    c20->cd(2);
    m_hists->GetHistFromMap("correctedXY_FCRC")->GetXaxis()->SetTitle("X [cm]");
    m_hists->GetHistFromMap("correctedXY_FCRC")->GetYaxis()->SetTitle("Y [cm]");
    gPad->SetLogz();
    m_hists->GetHistFromMap("correctedXY_FCRC")->SetStats(0);
    m_hists->GetHistFromMap("correctedXY_FCRC")->Draw("COLZ");
    
    c20->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/XY_FCRC_comparison.png").c_str());
    
    TF1 *linfit = new TF1 ("linfit", "[0] + [1]*x", -900, -50);
    TCanvas *c21 = new TCanvas();        
    m_hists->GetHistFromMap("driftTime_ns")->GetXaxis()->SetTitle("drift [us]");
    // gPad->SetLogy();
    m_hists->GetHistFromMap("driftTime_ns")->Fit("linfit", "R+");
    m_hists->GetHistFromMap("driftTime_ns")->SetStats(0);
    m_hists->GetHistFromMap("driftTime_ns")->Draw();
    c21->SaveAs(string("/global/u2/e/elmzweig/ALPACA/run/FiducialMass/" + datarange + "/driftTime100.png").c_str());
    cout<<"dT100 fit = az+b, a = "<<(linfit->GetParameter(1))<<", b = "<<(linfit->GetParameter(0))<<"."<<endl;
    
}
