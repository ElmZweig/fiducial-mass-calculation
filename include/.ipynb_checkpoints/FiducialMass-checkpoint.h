#ifndef FiducialMass_H
#define FiducialMass_H

#include "Analysis.h"

#include "CutsFiducialMass.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class FiducialMass : public Analysis {

public:
    FiducialMass();
    ~FiducialMass();

    void Initialize();
    void Execute();
    void Finalize();

protected:
    CutsFiducialMass* m_cutsFiducialMass;
    ConfigSvc* m_conf;
};

#endif
