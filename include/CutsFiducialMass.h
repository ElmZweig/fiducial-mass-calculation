#ifndef CutsFiducialMass_H
#define CutsFiducialMass_H

#include "EventBase.h"

class CutsFiducialMass {

public:
    CutsFiducialMass(EventBase* eventBase);
    ~CutsFiducialMass();
    bool FiducialMassCutsOK();
    bool SS();

private:
    EventBase* m_event;
};

#endif
